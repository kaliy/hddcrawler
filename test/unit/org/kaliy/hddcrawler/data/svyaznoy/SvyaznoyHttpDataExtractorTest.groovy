package org.kaliy.hddcrawler.data.svyaznoy

import org.kaliy.hddcrawler.domain.HddType
import spock.lang.Shared
import spock.lang.Specification

class SvyaznoyHttpDataExtractorTest extends Specification {

    @Shared
    SvyaznoyHttpDataExtractor extractor

    def setup() {
        extractor = new SvyaznoyHttpDataExtractor()
    }

    def "HDD links extractor returns list of available svyaznoy HDD display pages"() {
        given: "Source page with 4 pages total"
        def html = readFile('hdd_list_4_pages.html')

        when:
        def extracted = extractor.extractUrlsOfHddLists(html)

        then:
        extracted.size() == 3
        'http://www.svyaznoy.ru/catalog/flash/230?PAGEN_1=2' in extracted
        'http://www.svyaznoy.ru/catalog/flash/230?PAGEN_1=3' in extracted
        'http://www.svyaznoy.ru/catalog/flash/230?PAGEN_1=4' in extracted
    }

    def "HDD links extractor returns empty list if there are no links to other pages"() {
        given: "Source page without other list pages"
        def html = readFile('hdd_list_single_page.html')

        when:
        def extracted = extractor.extractUrlsOfHddLists(html)

        then:
        extracted.isEmpty()
    }

    def "HDD urls extractor extracts URLs to specific HDD pages"() {
        given: "Source page with 3 HDD urls"
        def html = readFile('hdd_list_3_hdd.html')

        when:
        def extracted = extractor.extractUrlsOfHdd(html)

        then:
        extracted.size() == 3
        'http://www.svyaznoy.ru/catalog/flash/230/2243795' in extracted
        'http://www.svyaznoy.ru/catalog/flash/230/2287190' in extracted
        'http://www.svyaznoy.ru/catalog/flash/230/2287172' in extracted
    }

    def "HDD data extractor extracts dimensions from HDD page"() {
        given: "Source page with External HDD  data"
        def html = readFile('hdd_page_external_hdd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.width == 119
        extracted.height == 21
        extracted.depth == 79
    }

    def "HDD data extractor extracts weight from HDD page"() {
        given: "Source page with External HDD  data"
        def html = readFile('hdd_page_external_hdd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.weight == 231
    }

    def "HDD data extractor extracts brand from HDD page"() {
        given: "Source page with External HDD  data"
        def html = readFile('hdd_page_external_hdd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.brand == "Toshiba"
    }

    def "HDD data extractor extracts production code from HDD page"() {
        given: "Source page with External HDD  data"
        def html = readFile('hdd_page_external_hdd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.productionCode == "HDTB320EK3CA"
    }

    def "HDD data extractor extracts capacity from HDD page"() {
        given: "Source page with External HDD  data"
        def html = readFile('hdd_page_external_hdd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.capacity == 2000L*1024*1024*1024
    }

    def "HDD data extractor extracts HDD type from HDD page"() {
        given: "Source page with External HDD  data"
        def html = readFile('hdd_page_external_hdd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.type == HddType.HDD
    }

    def "HDD data extractor extracts SSD type from SSD page"() {
        given: "Source page with SSD data"
        def html = readFile('hdd_page_ssd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.type == HddType.SSD
    }

    def "HDD data extractor extracts decimal dimensions from drive page"() {
        given: "Source page with SSD data"
        def html = readFile('hdd_page_ssd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.width == 99.8f
        extracted.height == 7.01f
        extracted.depth == 69.8f
    }

    def "HDD data extractor extracts decimal weight from drive page"() {
        given: "Source page with SSD data"
        def html = readFile('hdd_page_ssd.html')

        when:
        def extracted = extractor.extractHddData(html)

        then:
        extracted.weight == 52.2f
    }


    def "HDD data extractor fills HDD with null values if no information is present on page"() {
        given: "Source page with no technical specifications"
        def html = readFile('hdd_page_no_tech_specs.html')

        when:
        def hdd = extractor.extractHddData(html)

        then:
        hdd.width == null
        hdd.depth == null
        hdd.height == null
        hdd.type == null
        hdd.productionCode == null
        hdd.brand == null
        hdd.capacity == null
        hdd.weight == null

    }


    def private readFile(name) {
        getClass().classLoader.getResource("test/unit/resources/svyaznoy/$name").text
    }
}
