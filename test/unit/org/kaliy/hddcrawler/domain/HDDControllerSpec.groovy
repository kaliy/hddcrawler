package org.kaliy.hddcrawler.domain



import grails.test.mixin.*
import spock.lang.*

@TestFor(HDDController)
@Mock(HDD)
class HDDControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.HDDInstanceList
            model.HDDInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.HDDInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def HDD = new HDD()
            HDD.validate()
            controller.save(HDD)

        then:"The create view is rendered again with the correct model"
            model.HDDInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            HDD = new HDD(params)

            controller.save(HDD)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/HDD/show/1'
            controller.flash.message != null
            HDD.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def HDD = new HDD(params)
            controller.show(HDD)

        then:"A model is populated containing the domain instance"
            model.HDDInstance == HDD
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def HDD = new HDD(params)
            controller.edit(HDD)

        then:"A model is populated containing the domain instance"
            model.HDDInstance == HDD
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/HDD/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def HDD = new HDD()
            HDD.validate()
            controller.update(HDD)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.HDDInstance == HDD

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            HDD = new HDD(params).save(flush: true)
            controller.update(HDD)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/HDD/show/$HDD.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/HDD/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def HDD = new HDD(params).save(flush: true)

        then:"It exists"
            HDD.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(HDD)

        then:"The instance is deleted"
            HDD.count() == 0
            response.redirectedUrl == '/HDD/index'
            flash.message != null
    }
}
