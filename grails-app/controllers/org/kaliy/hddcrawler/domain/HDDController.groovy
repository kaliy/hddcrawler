package org.kaliy.hddcrawler.domain



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class HDDController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond HDD.list(params), model:[HDDInstanceCount: HDD.count()]
    }

    def show(HDD HDDInstance) {
        respond HDDInstance
    }

    def create() {
        respond new HDD(params)
    }

    @Transactional
    def save(HDD HDDInstance) {
        if (HDDInstance == null) {
            notFound()
            return
        }

        if (HDDInstance.hasErrors()) {
            respond HDDInstance.errors, view:'create'
            return
        }

        HDDInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'HDD.label', default: 'HDD'), HDDInstance.id])
                redirect HDDInstance
            }
            '*' { respond HDDInstance, [status: CREATED] }
        }
    }

    def edit(HDD HDDInstance) {
        respond HDDInstance
    }

    @Transactional
    def update(HDD HDDInstance) {
        if (HDDInstance == null) {
            notFound()
            return
        }

        if (HDDInstance.hasErrors()) {
            respond HDDInstance.errors, view:'edit'
            return
        }

        HDDInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'HDD.label', default: 'HDD'), HDDInstance.id])
                redirect HDDInstance
            }
            '*'{ respond HDDInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(HDD HDDInstance) {

        if (HDDInstance == null) {
            notFound()
            return
        }

        HDDInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'HDD.label', default: 'HDD'), HDDInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'HDD.label', default: 'HDD'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
