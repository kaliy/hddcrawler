
<%@ page import="org.kaliy.hddcrawler.domain.HDD" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'HDD.label', default: 'HDD')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-HDD" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-HDD" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list HDD">
			
				<g:if test="${HDDInstance?.brand}">
				<li class="fieldcontain">
					<span id="brand-label" class="property-label"><g:message code="HDD.brand.label" default="Brand" /></span>
					
						<span class="property-value" aria-labelledby="brand-label"><g:fieldValue bean="${HDDInstance}" field="brand"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${HDDInstance?.productionCode}">
				<li class="fieldcontain">
					<span id="productionCode-label" class="property-label"><g:message code="HDD.productionCode.label" default="Production Code" /></span>
					
						<span class="property-value" aria-labelledby="productionCode-label"><g:fieldValue bean="${HDDInstance}" field="productionCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${HDDInstance?.weight}">
				<li class="fieldcontain">
					<span id="weight-label" class="property-label"><g:message code="HDD.weight.label" default="Weight" /></span>
					
						<span class="property-value" aria-labelledby="weight-label"><g:fieldValue bean="${HDDInstance}" field="weight"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${HDDInstance?.height}">
				<li class="fieldcontain">
					<span id="height-label" class="property-label"><g:message code="HDD.height.label" default="Height" /></span>
					
						<span class="property-value" aria-labelledby="height-label"><g:fieldValue bean="${HDDInstance}" field="height"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${HDDInstance?.width}">
				<li class="fieldcontain">
					<span id="width-label" class="property-label"><g:message code="HDD.width.label" default="Width" /></span>
					
						<span class="property-value" aria-labelledby="width-label"><g:fieldValue bean="${HDDInstance}" field="width"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${HDDInstance?.depth}">
				<li class="fieldcontain">
					<span id="depth-label" class="property-label"><g:message code="HDD.depth.label" default="Depth" /></span>
					
						<span class="property-value" aria-labelledby="depth-label"><g:fieldValue bean="${HDDInstance}" field="depth"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${HDDInstance?.capacity}">
				<li class="fieldcontain">
					<span id="capacity-label" class="property-label"><g:message code="HDD.capacity.label" default="Capacity" /></span>
					
						<span class="property-value" aria-labelledby="capacity-label"><g:fieldValue bean="${HDDInstance}" field="capacity"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${HDDInstance?.type}">
				<li class="fieldcontain">
					<span id="type-label" class="property-label"><g:message code="HDD.type.label" default="Type" /></span>
					
						<span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${HDDInstance}" field="type"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:HDDInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${HDDInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
