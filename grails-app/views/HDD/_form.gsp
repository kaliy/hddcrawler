<%@ page import="org.kaliy.hddcrawler.domain.HDD" %>



<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'brand', 'error')} ">
	<label for="brand">
		<g:message code="HDD.brand.label" default="Brand" />
		
	</label>
	<g:textField name="brand" value="${HDDInstance?.brand}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'productionCode', 'error')} ">
	<label for="productionCode">
		<g:message code="HDD.productionCode.label" default="Production Code" />
		
	</label>
	<g:textField name="productionCode" value="${HDDInstance?.productionCode}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'weight', 'error')} ">
	<label for="weight">
		<g:message code="HDD.weight.label" default="Weight" />
		
	</label>
	<g:field name="weight" value="${fieldValue(bean: HDDInstance, field: 'weight')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'height', 'error')} ">
	<label for="height">
		<g:message code="HDD.height.label" default="Height" />
		
	</label>
	<g:field name="height" value="${fieldValue(bean: HDDInstance, field: 'height')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'width', 'error')} ">
	<label for="width">
		<g:message code="HDD.width.label" default="Width" />
		
	</label>
	<g:field name="width" value="${fieldValue(bean: HDDInstance, field: 'width')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'depth', 'error')} ">
	<label for="depth">
		<g:message code="HDD.depth.label" default="Depth" />
		
	</label>
	<g:field name="depth" value="${fieldValue(bean: HDDInstance, field: 'depth')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'capacity', 'error')} ">
	<label for="capacity">
		<g:message code="HDD.capacity.label" default="Capacity" />
		
	</label>
	<g:field name="capacity" type="number" value="${HDDInstance.capacity}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: HDDInstance, field: 'type', 'error')} ">
	<label for="type">
		<g:message code="HDD.type.label" default="Type" />
		
	</label>
	<g:select name="type" from="${org.kaliy.hddcrawler.domain.HddType?.values()}" keys="${org.kaliy.hddcrawler.domain.HddType.values()*.name()}" value="${HDDInstance?.type?.name()}"  noSelection="['': '']"/>

</div>

