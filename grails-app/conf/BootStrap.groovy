import org.kaliy.hddcrawler.data.svyaznoy.SvyaznoyHddDataProvider
import org.kaliy.hddcrawler.domain.HDD

class BootStrap {


    def init = { servletContext ->
        SvyaznoyHddDataProvider provider = new SvyaznoyHddDataProvider()
        provider.fetchHdds().each { HDD hdd ->
            hdd.save(failOnError: true, flush: true)
        }
    }
    def destroy = {
    }
}
