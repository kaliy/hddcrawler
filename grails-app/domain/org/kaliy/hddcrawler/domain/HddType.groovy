package org.kaliy.hddcrawler.domain

enum HddType {
    SSD, HDD
}
