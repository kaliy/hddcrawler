package org.kaliy.hddcrawler.domain

class HDD {
    String brand
    String productionCode
    Float weight
    Float height
    Float width
    Float depth
    Long capacity
    HddType type

    String toString() {
        "$type: $brand - $productionCode"
    }

    static constraints = {
        brand nullable: true
        productionCode  nullable: true
        weight  nullable: true, scale: 2
        height nullable: true, scale: 2
        width nullable: true, scale: 2
        depth nullable: true, scale: 2
        capacity nullable: true
        type nullable: true
    }
}
