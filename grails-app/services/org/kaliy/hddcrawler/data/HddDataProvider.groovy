package org.kaliy.hddcrawler.data

import org.kaliy.hddcrawler.domain.HDD

interface HddDataProvider {
    List<HDD> fetchHdds()
}
