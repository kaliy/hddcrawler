package org.kaliy.hddcrawler.data.svyaznoy

import groovy.util.logging.Slf4j
import org.kaliy.hddcrawler.data.HddDataProvider
import org.kaliy.hddcrawler.domain.HDD

@Slf4j
class SvyaznoyHddDataProvider implements HddDataProvider {

//    @Inject
    private SvyaznoyHttpDataExtractor extractor = new SvyaznoyHttpDataExtractor()

    @Override
    List<HDD> fetchHdds() {
        def urlsToCrawl = ["http://www.svyaznoy.ru/catalog/flash/230/"]
        urlsToCrawl += fetchAllHddListsPageUrls()
        println "Will check these lists: $urlsToCrawl"
        def pagesUrls = fetchAllHddPagesUrls(urlsToCrawl)
        println "Will check ${pagesUrls.size()} pages"
        def hdds = []
        pagesUrls.each {
            println "Checking $it"
            def hdd = extractor.extractHddData(it.toURL().getText("windows-1251"))
            println "Extracted $hdd"
            hdds << hdd
        }
        hdds
    }

    private def fetchAllHddPagesUrls(List<String> hddListsUrls) {
        def hddUrls = [] as Set
        hddListsUrls.each { fetchHddPagesUrls(it).each{hddUrls << it} }
        hddUrls
    }

    private def fetchHddPagesUrls(String url) {
        println "Fetching $url"
        def html = url.toURL().getText("windows-1251")
        def urls = extractor.extractUrlsOfHdd(html)
        println "Extracted ${urls.size()} urls"
        urls
    }

    private def fetchAllHddListsPageUrls() {
        println "Fetching page lists"
        def base = "http://www.svyaznoy.ru/catalog/flash/230/".toURL().getText("windows-1251")
        def urls = extractor.extractUrlsOfHddLists(base)
        println "Found ${urls.size()} links"
        urls
    }

}
