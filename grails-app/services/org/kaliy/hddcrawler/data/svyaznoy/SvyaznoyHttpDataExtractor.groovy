package org.kaliy.hddcrawler.data.svyaznoy

import org.cyberneko.html.parsers.SAXParser
import org.kaliy.hddcrawler.domain.HDD
import org.kaliy.hddcrawler.domain.HddType

class SvyaznoyHttpDataExtractor {

    def extractLinksWhichStartsWithCatalog = { parentDiv, result ->
        parentDiv.'**'.A.each {
            def link = it.'@href'
            if (link?.startsWith('/catalog')) {
                result << "http://www.svyaznoy.ru$link".toString()
            }
        }
    }

    HDD extractHddData(String html) {
        def parser = new SAXParser()
        def hdd = new HDD()
        new XmlParser(parser).parseText(html).with { page ->
            page.'**'.SPAN.findAll { it.'@class'?.contains 'ico-hint'}.each {
                it.parent().remove(it)
            }
            def data = [:]
            page.'**'.DIV.findAll { it.'@class'?.contains 'config-block'}.each {
                it.DIV.each {
                    def textData = it.text().replace('\n','').split(':')
                    if (textData.length > 1) {
                        data[textData[0].trim()] = textData[1].trim()
                    }
                }
            }
            hdd.width = data["Высота (мм)"] as Float
            hdd.depth = data["Ширина (мм)"] as Float
            hdd.height = data["Толщина (мм)"] as Float
            hdd.type = data["Тип носителя"] ? HddType.valueOf(data["Тип носителя"]) : null
            hdd.productionCode = data["Код производителя"]
            hdd.brand = data["Производитель"]
            hdd.capacity = data["Объем памяти (Гб)"] ? (data["Объем памяти (Гб)"] as Long) * 1024 * 1024 * 1024 : null
            hdd.weight = data["Вес (г)"] as Float
        }
        hdd
    }

    List<String> extractUrlsOfHdd(String  html) {
        def result = [] as Set
        def parser = new SAXParser()
        new XmlParser(parser).parseText(html).with { page ->
            page.'**'.DIV.findAll {
                it.'@class'?.contains 'listLineInfoName'}.each{extractLinksWhichStartsWithCatalog(it, result)
            }
        }
        result as List
    }

    List<String> extractUrlsOfHddLists(String html) {
        def result = [] as Set
        def parser = new SAXParser()
        new XmlParser(parser).parseText(html).with { page ->
            page.'**'.DIV.find {
                it.'@class'?.contains 'pagesPage'}.each{extractLinksWhichStartsWithCatalog(it, result)
            }
        }
        result as List
    }
}
